package pros.xgo.mybatiscrud.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import pros.xgo.mybatiscrud.entity.BookEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRepositoryTest {

	@Autowired
	private BookRepository bookRepository;

	@Test
	public void findAllTest() {
		List<BookEntity> bookList = bookRepository.findAll();
		System.out.println(bookList);
		Assert.assertTrue(bookList.size() > 0);
	}

	@Test
	@Transactional
	public void save() {
		BookEntity book = new BookEntity();
		book.setTitle("Spring In Action");
		book.setAuthor("Cralg Walls");
		book.setIsbn("ISBN 978-7-115-31606-6");
		book.setDescription("spring framework 入门比较好的书！");
		book.setReader("XGo");

		bookRepository.save(book);
		System.out.println(book);
		Assert.assertNotNull(book.getIsbn());
		
		throw new  IllegalStateException("模拟错误");
	}

}
