package pros.xgo.mybatiscrud.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pros.xgo.mybatiscrud.entity.BookEntity;

@Repository
public interface BookRepository {

	public List<BookEntity> findAll();

	public Integer save(BookEntity book);

	public Integer update(BookEntity book);

	public Integer delete(Long id);

}
