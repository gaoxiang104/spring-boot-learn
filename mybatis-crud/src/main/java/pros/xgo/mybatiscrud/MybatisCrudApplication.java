package pros.xgo.mybatiscrud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableTransactionManagement
@MapperScan(basePackages = { "pros.xgo.mybatiscrud.repository" })
public class MybatisCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatisCrudApplication.class, args);
	}
}
