package pros.xgo.hello;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/readingList")
public class ReadingListController {

	@Resource
	ReadingListRepository readingListRepository;

	@RequestMapping(value = "/{reader}", method = RequestMethod.GET)
	public String readersBooks(@PathVariable String reader, Model model) {
		List<BookEntity> books = readingListRepository.findByReader(reader);
		if (null != books && books.size() > 0) {
			model.addAttribute("books", books);
		}
		return "readingList";
	}

	@RequestMapping(value = "/{reader}", method = RequestMethod.POST)
	public String addToReadingList(@PathVariable String reader, BookEntity book) {
		book.setReader(reader);

		readingListRepository.save(book);

		return "redirect:/readingList/{reader}";
	}
}
