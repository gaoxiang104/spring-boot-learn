package pros.xgo.hello;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BookEntity implements Serializable {

	private static final long serialVersionUID = 6101123860401258057L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String reader;

	private String isbn;

	private String title;

	private String author;

	private String description;

	public String getAuthor() {
		return author;
	}

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getReader() {
		return reader;
	}

	public String getTitle() {
		return title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setReader(String reader) {
		this.reader = reader;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{\"id\":\"");
		builder.append(id);
		builder.append("\",\"reader\":\"");
		builder.append(reader);
		builder.append("\",\"isbn\":\"");
		builder.append(isbn);
		builder.append("\",\"title\":\"");
		builder.append(title);
		builder.append("\",\"author\":\"");
		builder.append(author);
		builder.append("\",\"description\":\"");
		builder.append(description);
		builder.append("\"} ");
		return builder.toString();
	}

}
