package pros.xgo.hello;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReadingListRepository extends JpaRepository<BookEntity, Long> {
	
	List<BookEntity> findByReader(String reader);
	
}
