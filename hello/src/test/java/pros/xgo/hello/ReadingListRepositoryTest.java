package pros.xgo.hello;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReadingListRepositoryTest {

	@Autowired
	private ReadingListRepository readingListRepository;
	
	@Test
	public void saveTest(){
		BookEntity book = new BookEntity();
		book.setTitle("Spring In Action");
		book.setAuthor("Cralg Walls");
		book.setIsbn("ISBN 978-7-115-31606-6");
		book.setDescription("spring framework 入门比较好的书！");
		book.setReader("XGo");
		
		readingListRepository.save(book);
		System.out.println(book);
		Assert.assertNotNull(book.getIsbn());
	}
	
	@Test
	public void findByReaderTest(){
		List<BookEntity> books = readingListRepository.findByReader("XGo");
		System.out.println(books);
		Assert.assertNotNull(books);}
}
